//
//  main.swift
//  vgc-checker
//
//  Created by Michael Graziano on 2/15/17.
//  Copyright © 2017 Michael Graziano. All rights reserved.
//

import Foundation

let sema = DispatchSemaphore(value: 0)
let url = URL(string: "https://mygift.giftcardmall.com/Card/_Login?returnUrl=Transactions")
var cardsChecked = 0
var flag = 0

struct CreditCard {
    let number: String
    let expMonth: String
    let expYear: String
    let cvvCode: String
    
    init?(with swipe: String, cvv: String) {
        guard let separatorIndex = swipe.characters.index(of: ";") else {
            return nil
        }
        
        let realIndex = swipe.index(separatorIndex, offsetBy: 1)
        let ccString = swipe.substring(from: realIndex)
        
        guard let equalsIndex = ccString.characters.index(of: "=") else { return nil }
        
        number = ccString.substring(to: equalsIndex)
        
        let expirationYearIndex = ccString.index(equalsIndex, offsetBy: 1)
        let expirationMonthStartIndex = ccString.index(expirationYearIndex, offsetBy: 2)
        let expirationMonthEndIndex = ccString.index(expirationMonthStartIndex, offsetBy: 2)
        expYear = ccString[expirationYearIndex ..< expirationMonthStartIndex]
        expMonth = ccString[expirationMonthStartIndex ..< expirationMonthEndIndex]
        cvvCode = cvv
    }
}

var cardsToCheck: [CreditCard] = []

func checkNextCard() {
    if cardsChecked == cardsToCheck.count {
        print("")
        print("****************************************************************************************")
        print("")
        begin()
        return
    }
    
    checkCard(card: cardsToCheck[cardsChecked], onCompletion: {
        cardsChecked += 1
        checkNextCard()
    })
}

func begin() {
    
    cardsChecked = 0
    print("Swipe your card now, or type 'y' to check all saved cards:")
    if let swipe = readLine() {
        
        if swipe.characters.count < 20 {
            if swipe == "y" {
                print("")
                print("****************************************************************************************")
                print("")
                checkNextCard()
            } else if swipe == "r" {
                print("Enter card number to remove:")
                let removeIndex = Int(readLine()!)
                
                if let checkedIndex = removeIndex, checkedIndex < cardsToCheck.count {
                    cardsToCheck.remove(at: checkedIndex)
                }
                
                begin()
            } else if swipe == "ra" {
                cardsToCheck.removeAll()
                print("All cards removed")
                begin()
            } else if swipe == "load" {
                loadCsv(fileName: "giftcards")
                begin()
            } else {
                begin()
            }
            return
        }
        
        print("Enter CVV:")
        
        if let cvv = readLine(),
            cvv.characters.count == 3,
            let creditCard = CreditCard(with: swipe, cvv: cvv) {
            
            checkCard(card: creditCard, onCompletion: {
                if cardsToCheck.filter({ $0.number == creditCard.number }).count == 0 {
                    cardsToCheck.append(creditCard)
                }
                begin()
            })
            
        } else {
            print("Invalid CVV\n")
            begin()
            return
        }
    }
}

func loadCsv(fileName: String) {
    cardsToCheck.append(contentsOf: importFromCSV(filePath: fileName))
}

func importFromCSV(filePath: String) -> [CreditCard] {
    
    let pathTemp = "giftcards.csv"
    
    if let dir = FileManager.default.urls(for: .downloadsDirectory, in: .userDomainMask).first {
        let path = dir.appendingPathComponent(pathTemp)
        
        do {
            
            let csvData = try String(contentsOf: path, encoding: String.Encoding.utf8)

            var loadedGiftCards: [CreditCard] = []
            let csv = csvData.csvRows()

            for row in csv {
                if let newCard = CreditCard(with: row[2], cvv: row[1]) {
                    if loadedGiftCards.filter({ $0.number == newCard.number }).count == 0 && cardsToCheck.filter({ $0.number == newCard.number }).count == 0 { loadedGiftCards.append(newCard) }
                }
            }
            
            print("Loaded \(loadedGiftCards.count) records")
            return loadedGiftCards
        } catch {
            print(error)
            return []
        }
    }
    return []
}

func checkCard(card: CreditCard, onCompletion: @escaping () -> Void) {
    let postString = "CardNumber=\(card.number)&ExpirationMonth=\(card.expMonth)&ExpirationYear=\(card.expYear)&SecurityCode=\(card.cvvCode)"
    
    let xGonGiveItToYa = "XMLHttpRequest"
    var request = URLRequest(url: url!)
    request.httpMethod = "POST"
    request.httpBody = postString.data(using: .utf8)
    request.addValue(xGonGiveItToYa, forHTTPHeaderField: "X-Requested-With")
    
    let session = URLSession.shared
    
    _ = session.dataTask(with: request) { (data, response, error) in
        
        if error != nil {
            print("ERROR, TRY AGAIN")
            return
        }
        
        let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
        
        guard let lowerBound = strData?.range(of: "Available Balance").toRange()?.lowerBound,
            let valueString = strData?.substring(from: lowerBound) as NSString?,
            let lowerAmtBound = valueString.range(of: "$").toRange()?.lowerBound,
            let valueString2 = valueString.substring(from: lowerAmtBound) as NSString?,
            let lowerDivBound = valueString2.range(of: "</div>").toRange()?.lowerBound,
            let finalValue = valueString2.substring(to: lowerDivBound) as NSString?
            else {
                print("ERROR, TRY AGAIN")
                onCompletion()
                return
        }
        
        var stringVal = finalValue as String
        if stringVal.characters.last == ")" {
            stringVal = "-\(finalValue)"
            stringVal = String(stringVal.characters.dropLast())
        }
        let index = cardsToCheck.index(where: { $0.number == card.number })
        let indexToPrint = index != nil ? "\(index!))" : "Card Number:"
        print("\(indexToPrint) \(String(card.number.characters.suffix(4)))    Value: \(stringVal)")
        onCompletion()
        }.resume()
}

func checkCardsInArray(array: [CreditCard]) {
    
}

begin()

sema.wait()

